module Refinery
  module Services
    class Service < Refinery::Core::BaseModel
      self.table_name = 'refinery_services'

      attr_accessible :title, :body, :position

      acts_as_indexed :fields => [:title, :body]

      validates :title, :presence => true, :uniqueness => true
    end
  end
end
