SlideShow =
  current_image: 0,
  max_images: 0,

  init: ->
    $slideshow = $('#slideshow > div ul')
    $slideshow.find('li').hide().eq(0).show()
    @max_images = $slideshow.find('li').length

    @change_slide($slideshow)
    @build_links($slideshow)

    @change_caption($slideshow.find('li:eq(' + SlideShow.current_image + ')'))
    $("body").on("click", "#slideshow_nav li", @go_to_slide)

  change_slide: (slideshow) ->
    SlideShow.myInterval = setInterval ->
      slideshow.find('li').hide()
      $nav = $("#slideshow_nav")
      $nav.find('li').removeClass('activeSlide')

      if SlideShow.current_image + 1 < SlideShow.max_images
        slideshow.find('li:eq(' + (SlideShow.current_image  + 1) + ')').fadeIn()
        SlideShow.current_image++
        $nav.find('li:eq(' + ( SlideShow.current_image ) + ')').addClass('activeSlide')
      else
        SlideShow.current_image = 0
        slideshow.find('li:eq(0)').fadeIn()
        $nav.find('li:eq(0)').addClass('activeSlide')

      SlideShow.change_caption(slideshow.find('li:eq(' + SlideShow.current_image + ')'))
    , 3500

  change_caption: (slide) ->
    $("#slideshow_caption").html(slide.data('caption'))

  build_links: (slideshow) ->
    $nav = $("#slideshow_nav")
    slideshow.find('li').each (i, e) ->
      $nav.append("<li class='slide"+(i+1)+"' data-slide-id='"+i+"'><a href='#'></a></li>")
    $nav.find('li:eq(0)').addClass('activeSlide')

  go_to_slide: (e) ->
    e.preventDefault()
    window.clearInterval(SlideShow.myInterval)
    slideshow = $('#slideshow > div ul')
    slide_id = $(this).data('slide-id')
    SlideShow.current_image = slide_id
    $("#slideshow_nav").find('li').removeClass('activeSlide')
    $("#slideshow_nav").find('li:eq(' + ( SlideShow.current_image ) + ')').addClass('activeSlide')
    SlideShow.change_caption( slideshow.find('li:eq(' + SlideShow.current_image + ')') )

    slideshow.find('li').hide()
    slideshow.find('li:eq(' + SlideShow.current_image + ')').fadeIn()
    SlideShow.change_slide(slideshow)
    SlideShow.current_image++

$ ->
  if window.location.pathname == "/"
    SlideShow.init()
